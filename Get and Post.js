import React,{Component} from 'react';


class App extends Component{


    async postData(){
        try{
            let result = await fetch('http://10.10.88.182:8080/bonita/loginservice',{
                method : 'post',
                mode : 'no-cors',
                headers:{
                    'Accept' : 'application/x-www-form-urlencoded',
                    'Content-type' : 'application/x-www-form-urlencoded',
                }
            })
            console.log('Hasil Post ' + result)
        }
        catch(e){
            console.log(e);
        }
    }

    componentDidMount(){

        //let proxyURL = "https://cors-anywhere.herokuapp.com/";
        fetch('http://10.10.88.182:8080/bonita/API/bpm/process?p=0&c=10',{
            method : 'get',
            headers : {
                'Accept' : 'application/json',
                'content-type' :'application/json',
                'Authorization' : 'Bearer'
            }
        })
        .then( res => {
        if(res.status === 200)
                return res.json()
                
        })
        .then( json => {
            this.setState({
                data: json
            })
            console.log('Hasil Get ' + this.state.data)
        })
   }

   render(){
       return(
           <div className = "App">
               <button onClick = {() => this.postData()}>POST</button>
               <button onClick = {() => this.componentDidMount()}>GET</button>
           </div>
       )
   }
}

export default App;